# Chef Technician

**Intended audience**: System administrators at companies adopting Chef.

**Course length**: 1 day.

The end product of this course is a sysadmin who can use Chef to configure
VMs, and can troubleshoot common problems.

This course was inspired by Chef's [Infrastructure Automation](https://learn.chef.io/tracks/infrastructure-automation#/) 
and [Chef Essentials](https://training.chef.io/instructor-led-training/chef-essentials) courses, but does not cover writing Chef code.

## Part I - Theory

- Chef Basic Terminology
  - Idempotency/convergence - test & repair model - how Chef enforces consistency across infrastructure
  - Infrastructure As Code
  - Desired State Configuration
  - Resources
  - Recipes
  - Cookbook
  - Chef Server
  - Search
  - Chef Client
  - Nodes
  - Run List
  - Roles
  - Environments

- Design Philosophy
  - Chef is written in Ruby (DSL, raw Ruby, libraries)
  - Explicit actions
  - Ordering of actions
  - Stop on failure
  - Chef client converge intervals, and how to "do it now!"


## Part II - Practical

### Learn the Chef basics

Get started by using Chef to keep your servers in line with the configuration policies you describe. You'll set up a web server and serve a basic home page on your own virtual machine or one that we provide.

- Set up a machine to manage
- Configure a resource
- Configure a package and service
- Make your recipe more manageable (create a cookbook)

### Manage a node with Chef server

Learn how the Chef server acts as a central repository for your cookbooks and for information about your servers, or nodes.

- Set up your workstation
- Install Chef Server
- Upload a cookbook to Chef Server
- Get a node to bootstrap
- Bootstrap your node
- Get familiar with the Chef Server UI
- Update your node's configuration
- Resolve a failed chef-client run
- Run chef-client periodically

### Go beyond the basics

Learn what happens behind the scenes when chef-client runs.

- What happens during knife bootstrap
- What happens during a chef-client run
- What is the security model used by chef-client
- Attribute precedence explained

### Troubleshooting

- Common problems and solutions